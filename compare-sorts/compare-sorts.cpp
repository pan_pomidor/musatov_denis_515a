﻿/**
*  @file  Project-git.cpp
*  @author Мусатов Д., гр. 515а
*  @serial 24.06.19
*  @brief  Навчальна практика
*
*  Проектування та розробка програми. Використання Git.
*/

#include "pch.h"
#include <stdio.h> 
#include <stdlib.h> 
#include <cstdlib> 
#include <malloc.h>
#include <math.h> 
#include <locale.h> 
#define N 1000
int cmp(const void *a, const void *b)
{
	return *(int*)a - *(int*)b;
}
void InsertionSort(int n, int mass[])
{
	int newElement, location; for (int i = 1; i < n; i++)
	{
		newElement = mass[i]; location = i - 1;
		while (location >= 0 && mass[location] > newElement)
		{
			mass[location + 1] = mass[location]; location = location - 1;
		}
		mass[location + 1] = newElement;
	}
}
void sorting(int n, int a[])
{
	int i, min, j, buf;
	for (i = 0; i < n - 1; i++) {

		//поиск минимального элемента в части массива от а[1] до a[SIZE]} 
		min = i;
		for (j = i + 1; j < n; j++)
			if (a[j] < a[min])
				min = j;

		//поменяем местами a [min] и a[i]
		buf = a[i];
		a[i] = a[min];
		a[min] = buf;
	}
}


int main()
{
	int n, i;
	int sort;
	int a[N];
	int* mass;
	printf("Choose the sort: \n");
	printf("1 - bubble sort \n");
	printf("2 - insertion sort \n");
	printf("3 - selection sort \n");
	scanf_s("%d", &sort);
	switch (sort)
	{
	case 1:
	{
		//Пузырьковая сортировка
		printf("Bubble sort\n");
		printf("Input amount of elements: ");
		scanf_s("%d", &n);
		printf("Input elements: \n");
		for (i = 0; i < n; i++)  // ЧИТАЕМ ВХОД
			scanf_s("%d", &a[i]);
		printf("Sorted array:\n");
		qsort(a, n, sizeof(int), cmp);
		// СОРТИРУЕМ 
		for (i = 0; i < n; i++) { // ВЫВОДИМ РЕЗУЛЬТАТ
			printf("%d ", a[i]);
		}break;
	case 2:
	{
		//Сортировка вставками 
		printf("Insertion sort\n");
		printf("Input amount of elements: ");
		scanf_s("%d", &n);
		//выделение памяти под массив

		mass = (int *)malloc(n * sizeof(int));
		//ввод элементов  массива 
		printf("Input the array elements:\n");
		for (int i = 0; i < n; i++)
			scanf_s("%d", &mass[i]);
		//сортировка вставками 
		InsertionSort(n, mass);
		//вывод отсортированного массива на экран   
		printf("Sorted array:\n");
		for (int i = 0; i < n; i++)
			printf("%d ", mass[i]);
		printf("\n");
		//освобождение памяти 
		free(mass);
		break;
	}
	case 3:
		//Сортировка выбором
		printf("Selection sort\n");
		printf("Input amount of elements: ");
		scanf_s("%d", &n);
		printf("Input array elements:\n");
		for (i = 0; i < n; i++) {
			scanf_s("%d", &a[i]);
		}
		//Вызов функции сортировки
		sorting(n, a);
		
		printf("Sorted array:\n");
		//Выведем массив
		for (i = 0; i < n; i++)
			printf("%d ", a[i]);
		printf("\n");
		break;
	}
	system("pause");
	}
}